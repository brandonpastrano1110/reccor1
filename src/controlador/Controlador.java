/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package controlador;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener; 
import javax.swing.JOptionPane;
import javax.swing.JFrame;
import modelo.Recibo;
import vista.dlgRecibo;
/**
/**
 *
 * @author Brandon
 */
public class Controlador implements ActionListener{
    private dlgRecibo vista;
    private Recibo rec;

    public Controlador(dlgRecibo vista, Recibo rec) {
        this.vista = vista;
        this.rec = rec;
        
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
    }
    public void iniciarVista(){
        vista.setTitle("----- RECIBO -----");
        vista.setSize(500,500);
        vista.setVisible(true);
    }
    public void limpiar(){
        vista.txtNumR.setText("");
        vista.txtFecha.setText("");
        vista.txtNombre.setText("");
        vista.txtDomicilio.setText("");
        vista.txtCosto.setText("");
        vista.txtKilCons.setText("");
        vista.cmbTipo.setSelectedIndex(0);
        vista.lblImp.setText("$ ");
        vista.lblSubtotal.setText("$ ");
        vista.lblTotal.setText("$ ");
        
    }
    public static void main(String[] args) {
        // TODO code application logic here
        Recibo rec = new Recibo();
        dlgRecibo vista= new dlgRecibo(new JFrame(), true);
        Controlador contra = new Controlador(vista, rec);
        contra.iniciarVista();
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==vista.btnGuardar){
            try {
                rec.setNombre(vista.txtNombre.getText());
                rec.setFecha(vista.txtFecha.getText());
                rec.setDomicilio(vista.txtDomicilio.getText());
                rec.setNumRecibo(Integer.parseInt(vista.txtNumR.getText()));

                rec.setKiCon(Float.parseFloat(vista.txtKilCons.getText()));
                rec.setTipoSer(vista.cmbTipo.getSelectedIndex());
                switch (rec.getTipoSer()) {
                    case 0:
                        rec.setCostoKi(2);
                        break;
                    case 1:
                        rec.setCostoKi(3);
                        break;
                    case 2:
                        rec.setCostoKi(5);
                        break;
                }
                JOptionPane.showMessageDialog(vista, "Se guardo con exito");
             }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error: "+ex.getMessage());
            }
            catch(Exception ex2){
                 JOptionPane.showMessageDialog(vista, "Surgio el siguiente error: "+ex2.getMessage());
            }
            //
            
        }
        if(e.getSource()==vista.btnMostrar){
            vista.txtNumR.setText(Integer.toString(rec.getNumRecibo()));
            vista.txtNombre.setText(rec.getNombre());
            vista.txtFecha.setText(rec.getFecha());
            vista.txtCosto.setText(Float.toString(rec.getCostoKi()));
            vista.txtKilCons.setText(Float.toString(rec.getKiCon()));
            vista.txtDomicilio.setText(rec.getDomicilio());
            vista.cmbTipo.setSelectedIndex(rec.getTipoSer());
            vista.lblImp.setText("$" +Float.toString(rec.calcularIm()));
            vista.lblSubtotal.setText("$ "+Float.toString(rec.calcularSubT()));
            vista.lblTotal.setText("$ "+Float.toString(rec.calcularTot()));
            
        }
        if(e.getSource()==vista.btnLimpiar){
            limpiar();
        }
        if(e.getSource()==vista.btnCancelar){
            limpiar();
            vista.txtNumR.setEnabled(false);
            vista.txtFecha.setEnabled(false);
            vista.txtNombre.setEnabled(false);
            vista.txtDomicilio.setEnabled(false);
            vista.txtCosto.setEnabled(false);
            vista.txtKilCons.setEnabled(false);
            vista.cmbTipo.setEnabled(false);
            vista.btnCancelar.setEnabled(false); 
            vista.btnLimpiar.setEnabled(false);
            vista.btnMostrar.setEnabled(false);
            vista.btnGuardar.setEnabled(false);
        }
        if(e.getSource()==vista.btnNuevo){
            vista.txtNumR.setEnabled(true);
            vista.txtFecha.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.txtDomicilio.setEnabled(true);
            vista.txtCosto.setEnabled(true);
            vista.txtKilCons.setEnabled(true);
            vista.cmbTipo.setEnabled(true);
            vista.btnCancelar.setEnabled(true); 
            vista.btnLimpiar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
            vista.btnGuardar.setEnabled(true);
        }
        if(e.getSource()==vista.btnCerrar){
            int option=JOptionPane.showConfirmDialog(vista,"¿Deseas salir?",
            "Decide", JOptionPane.YES_NO_OPTION);
             if(option==JOptionPane.YES_NO_OPTION){
                 vista.dispose();
                 System.exit(0);
             }
        }
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
}
