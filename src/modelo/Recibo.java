/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

/**
 *
 * @author Brandon
 */
public class Recibo {
    int numRecibo, tipoSer;
    String nombre, domicilio, fecha;
    float costoKi, kiCon;
    
    public Recibo(){
        numRecibo=0;
        tipoSer=0;
        nombre=""; 
        domicilio=""; 
        fecha="";
        costoKi=0.0f; 
        kiCon=0.0f;
    }
    public Recibo(Recibo rec){
        numRecibo=rec.numRecibo;
        tipoSer=rec.tipoSer;
        nombre=rec.nombre; 
        domicilio=rec.domicilio; 
        fecha=rec.fecha;
        costoKi=rec.costoKi; 
        kiCon=rec.kiCon;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public int getTipoSer() {
        return tipoSer;
    }

    public void setTipoSer(int tipoSer) {
        this.tipoSer = tipoSer;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public float getCostoKi() {
        return costoKi;
    }

    public void setCostoKi(float costoKi) {
        this.costoKi = costoKi;
    }

    public float getKiCon() {
        return kiCon;
    }

    public void setKiCon(float kiCon) {
        this.kiCon = kiCon;
    }
    
    public Recibo(int numRecibo, int tipoSer, String nombre, String domicilio, String fecha, float costoKi, float kiCon) {
        this.numRecibo = numRecibo;
        this.tipoSer = tipoSer;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.fecha = fecha;
        this.costoKi = costoKi;
        this.kiCon = kiCon;
    }
    
    public float calcularSubT(){
        return this.kiCon*this.costoKi;
    }
    public float calcularIm(){
        return (calcularSubT()/100)*16;
    }
    public float calcularTot(){
        return calcularSubT()+calcularIm();
    }
}
